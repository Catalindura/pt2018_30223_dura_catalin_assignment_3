package model;

public class Stoc {
	private int idstoc;
	private int cantitateStoc;
	
	public Stoc(int i,int c) {
		idstoc=i;
		cantitateStoc=c;
	}
	
	public Stoc() {
	}
	public int getIdstoc() {
		return idstoc;
	}
	public void setIdstoc(int x) {
		this.idstoc=x;
	}
	public int getCantitateStoc() {
		return cantitateStoc;
	}
	public void setCantitateStoc(int x) {
		this.cantitateStoc=x;
	}
	public String toString() {
		return "Id-ul produsului este "+idstoc+" iar stocul disponibil este:"+cantitateStoc;
	}
}
