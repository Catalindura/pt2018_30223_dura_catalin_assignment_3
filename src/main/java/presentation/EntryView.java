package presentation;

import java.awt.event.ActionListener;

import javax.swing.*;

public class EntryView extends JFrame {
	private JButton listareComanda=new JButton("Listare comenzi");
	private JButton client=new JButton("Client");
	private JButton produs=new JButton("Produs");
	private JButton comanda=new JButton("Comanda");
	private JButton factura=new JButton("Factura");
	private JButton clear=new JButton("Clear");
	private JTextField idFactura=new JTextField(3);
	private JTextField idComanda=new JTextField(3);
	private JTextField idClientComanda=new JTextField(3);
	private JTextField idProdusComanda=new JTextField(3);
	private JTextField cantitate=new JTextField(3);
	
	public EntryView() {
		JPanel p=new JPanel();
		p. setLayout (new BoxLayout (p, BoxLayout . Y_AXIS ));
		JPanel p1=new JPanel();
		p1.add(client);
		p.add(p1);
		JPanel p2=new JPanel();
		p2.add(produs);
		p.add(p2);
		JLabel l1=new JLabel(" idClient: ");
		JLabel l2=new JLabel(" idProdus ");
		JLabel l3=new JLabel(" cantitate ");
		JLabel l4=new JLabel(" id ");
		JPanel p3=new JPanel();
		p3.add(comanda);
		p3.add(l4);
		p3.add(idComanda);
		p3.add(l1);
		p3.add(idClientComanda);
		p3.add(l2);
		p3.add(idProdusComanda);
		p3.add(l3);
		p3.add(cantitate);
		p.add(p3);
		JPanel p4=new JPanel();
		p4.add(listareComanda);
		p4.add(factura);
		p4.add(l4);
		p4.add(idFactura);
		p.add(p4);
		JPanel p5=new JPanel();
		p5.add(clear);
		p.add(p5);
		this.add(p);
		this.setSize(500,500);
		this.setVisible(true);
		
	}
	
	public String getIdFactura() {
		return idFactura.getText();
	}
	public void setIdFactura() {
		idFactura.setText("");
	}
	void showMessage(String s) {
		JOptionPane.showMessageDialog(this,s);
	}
	public String getIdComanda() {
		return idComanda.getText();
	}
	public void setIdComanda() {
		idComanda.setText("");
	}
	public String getIdClientComanda() {
		return  idClientComanda.getText();
	}
	public void SetIdCLientComanda() {
		idClientComanda.setText("");
	}
	public String getIdProdusComanda() {
		return idProdusComanda.getText();
	}
	public void setIdProdusComanda() {
		idProdusComanda.setText("");
	}
	public String getCantitate() {
		return cantitate.getText();
	}
	public void SetCantitate() {
		cantitate.setText("");
	}
	void listareComenziListener(ActionListener mal) {
		listareComanda.addActionListener(mal);
	}
	void clientListener(ActionListener mal) {
		client.addActionListener(mal);
	}
	void produsListener(ActionListener mal) {
		produs.addActionListener(mal);
	}
	void comandaListener (ActionListener mal) {
		 comanda.addActionListener(mal);
	}
	void facturaListener(ActionListener mal) {
		factura.addActionListener(mal);
	}
	void clearListener(ActionListener mal) {
		clear.addActionListener(mal);
	}
}
