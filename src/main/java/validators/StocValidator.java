package validators;

import model.Stoc;

public class StocValidator implements Validator<Stoc> {
	
	public int validate(Stoc t) {
		if(t.getCantitateStoc()<=0)
			return 1;
		return 0;
	}
}