package validators;

import model.Client;

public class TelefonValidator implements Validator<Client> {
	
	public int validate(Client t) {
		if(t.getTelefon().length()!=10)
			return 1;
		if(t.getTelefon().charAt(0)!='0')
			return 1;
		return 0;
	}
}