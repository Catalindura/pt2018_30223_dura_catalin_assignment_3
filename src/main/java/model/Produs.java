package model;

public class Produs {
	private int id;
	private String nume;
	private int pret;
	public Produs() {
	}
	public Produs(int i,String n,int p) {
		super();
		id=i;
		nume=n;
		pret=p;
	}
	public int getId() {
		return id;
	}
	public void setId(int x) {
		this.id=x;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String x) {
		this.nume=x;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int x) {
		this.pret=x;
	}
	public String toString() {
		return "Id="+id+","+"Nume="+nume+","+"pret="+pret;
	}
}
