package model;

public class Client {
	private int id;
	private String nume;
	private String adresa;
	private String email;
	private String telefon;
	public Client() {
	}
	public Client(int i,String n,String a,String e,String t) {
		super();
		id=i;
		nume=n;
		adresa=a;
		email=e;
		telefon=t;
	}
	public int getId() {
		return id;
	}
	public void setId(int x) {
		this.id=x;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String x) {
		this.nume=x;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String x) {
		this.adresa=x;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String x) {
		email=x;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String x) {
		telefon=x;
	}
	public String toString() {
		return "Nume="+nume+", "+"Adresa="+adresa+", "+"Email-ul="+email+", "+"Numar de telefon="+telefon;
		
		
	}
}
