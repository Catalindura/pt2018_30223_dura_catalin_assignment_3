package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

public abstract class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}
	private String createUpdateQuery(String field) {
		StringBuilder sb=new StringBuilder();
		sb.append(" UPDATE ");
		sb.append(type.getSimpleName());
		sb.append(" SET ");
		for(Field f:type.getDeclaredFields()) {
			sb.append(f.getName()+"=?,");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(" WHERE "+field+"=?");
		return sb.toString();
	}
	
	private String createSelectQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}
	
	private String createSelectQueryBy(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + "=?");
		return sb.toString();
	}
	private String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		StringBuilder val=new StringBuilder();
		val.append("(");
		sb.append("INSERT ");
		sb.append(" INTO ");
		sb.append(type.getSimpleName());
		sb.append(" (");
		for (Field field : type.getDeclaredFields()) {
			sb.append(field.getName()+",");
			val.append("?,");
		}
		sb.deleteCharAt(sb.length()-1);
		val.deleteCharAt(val.length()-1);
		val.append(")");
		sb.append(") ");
		sb.append(" VALUES ");
		sb.append(val);
		return sb.toString();
	}

	private String createDeleteQueryBy(String field) {
		StringBuilder sb=new StringBuilder();
		sb.append(" DELETE FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE "+field+"=?");
		return sb.toString();
	}
	public List<T> findAll() {
		List<T> lista=new ArrayList<T>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			lista.addAll(createObjects(resultSet));
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return lista;
	}

	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQueryBy((type.getDeclaredFields()[0].getName()));
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			
			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public int deleteById(int id) {
		Connection connection=null;
		PreparedStatement statement=null;
		String query=createDeleteQueryBy((type.getDeclaredFields()[0].getName()));
		try {
			connection=ConnectionFactory.getConnection();
			statement=connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
			return 1;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:DeleteById " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return 0;
	}
	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public T insert(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createInsertQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			int i=1;
			for(Field field:type.getDeclaredFields()) {
				field.setAccessible(true);
				Object inserat=field.get(t);
				statement.setString(i,inserat.toString());
				i=i+1;
			}
			statement.executeUpdate();
			return t;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;

	}

	public T update(T t,int id) {
			Connection connection=null;
			PreparedStatement statement=null;
			String query=createUpdateQuery((type.getDeclaredFields()[0].getName()));
			try {
				connection=ConnectionFactory.getConnection();
				statement=connection.prepareStatement(query);
				int i=1;
				for(Field field:type.getDeclaredFields()) {
					field.setAccessible(true);
					Object inserat=field.get(t);
					statement.setString(i,inserat.toString());
					i=i+1;
				}
				statement.setInt(i, id);
				statement.executeUpdate();
				return t;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, type.getName() + "DAO:UpdateById " + e.getMessage());
			} finally {
				ConnectionFactory.close(statement);
				ConnectionFactory.close(connection);
			}
			return null;
	}
	}
