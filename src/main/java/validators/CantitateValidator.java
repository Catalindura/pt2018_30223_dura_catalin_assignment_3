package validators;

import model.Comanda;

public class CantitateValidator implements Validator<Comanda> {
	
	public int validate(Comanda t) {
		if(t.getCantitate()<=0)
			return 1;
		return 0;
	}
}