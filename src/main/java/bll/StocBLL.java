package bll;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import validators.StocValidator;
import validators.Validator;
import dao.StocDAO;
import model.Stoc;


public class StocBLL {
	private StocDAO stocDAO;
	private List<Validator<Stoc>> validators;
	
	public StocBLL() {
		validators=new ArrayList<Validator<Stoc>>();
		validators.add(new StocValidator());
		stocDAO=new StocDAO();
	}
	public Stoc findProdusStocById(int id) {
		Stoc st = stocDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("Produsul cu id-ul:"+id+"nu a fost gasit");
		}
		return st;
	}
	public int deleteProdusStocById(int id) {
		int st=stocDAO.deleteById(id);
		if(st==0) {
			throw new NoSuchElementException("Produsul cu id-ul:"+id+" nu a fost gasit");
		}
		return 1;
	}
	public Stoc UpdateProdusStocById(int id,Stoc t) {
		Stoc st=stocDAO.update(t, id);
		if(st==null) {
			throw new NoSuchElementException("Produsul cu id-ul:"+id+" nu a fost gasit");
		}
		return st;
	}
	public int InsertProdusStoc(Stoc t) {
		int sum=0;
		for (Validator<Stoc> v : validators) {
			sum=sum+v.validate(t);
		}
		if(sum==0) {
		 stocDAO.insert(t);
		 return 1;
		}
		else return 0;
	}
	public List<Object> findAll() {
		List<Object> lista=new ArrayList<Object>();
		lista.addAll(stocDAO.findAll());
		if(lista.size()==0) {
			throw new NoSuchElementException("Nu avem elemente in tabela");
		}
		return lista;
	}
	public static void main(String[] args){
		Stoc x=new Stoc(1,1);
		StocBLL test=new StocBLL();
		test.deleteProdusStocById(1);
		System.out.println(x.toString());
	}
}
