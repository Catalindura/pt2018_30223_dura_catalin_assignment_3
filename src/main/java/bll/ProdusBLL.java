package bll;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import validators.PretValidator;
import validators.Validator;
import dao.ProdusDAO;
import model.Produs;


public class ProdusBLL {
	private ProdusDAO produsDAO;
	private List<Validator<Produs>> validators;
	
	public ProdusBLL() {
		validators=new ArrayList<Validator<Produs>>();
		validators.add(new PretValidator());
		produsDAO=new ProdusDAO();
	}
	public Produs findProdusById(int id) {
		Produs st = produsDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("Produsul cu id-ul:"+id+"nu a fost gasit");
		}
		return st;
	}
	public int deleteProdusById(int id) {
		int st=produsDAO.deleteById(id);
		if(st==0) {
			throw new NoSuchElementException("Produsul cu id-ul:"+id+" nu a fost gasit");
		}
		return 1;
	}
	public Produs UpdateProdusById(int id,Produs t) {
		Produs st=produsDAO.update(t, id);
		if(st==null) {
			throw new NoSuchElementException("Produsul cu id-ul:"+id+" nu a fost gasit");
		}
		return st;
	}
	public int InsertProdus(Produs t) {
		int sum=0;
		for (Validator<Produs> v : validators) {
			sum=sum+v.validate(t);
		}
		if(sum==0) {
		 produsDAO.insert(t);
		 return 1;
		}
		else return 0;
	}
	public List<Object> findAll() {
		List<Object> lista=new ArrayList<Object>();
		lista.addAll(produsDAO.findAll());
		if(lista.size()==0) {
			throw new NoSuchElementException("Nu avem elemente in tabela");
		}
		return lista;
	}
	public static void main(String[] args){
		Produs x=new Produs(1,"Razer",450);
		Produs y=new Produs(2,"Lenovo",3333);
		Produs w=new Produs(4,"Alien ",-3000);
		ProdusBLL test=new ProdusBLL();
		List<Object> aux=new ArrayList<Object>();
		aux.addAll(test.findAll());
		for(java.lang.reflect.Field f :aux.getClass().getDeclaredFields()) {
			f.setAccessible(true);
		}
		}
}
