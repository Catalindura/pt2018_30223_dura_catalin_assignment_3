package bill;

import java.sql.Date;
import model.*;

public class Factura {
	public Client client;
	public Produs produs;
	public int cantitate;
	public java.util.Date data;
	
	public Factura(Client c,Produs p,int cant,java.util.Date date) {
		client=c;
		produs=p;
		cantitate=cant;
		data=date;
	}
	
	public String generareFactura() {
		String continut="Factura,id client:"+client.getId()+" \n";
		continut=continut+"Data comenzii:"+data.toString()+"\n";
		continut=continut+"Detalii client:"+"\n";
		continut=continut+"Nume:"+client.getNume()+"\n";
		continut=continut+"Adresa:"+client.getAdresa()+"\n";
		continut=continut+"Email:"+client.getEmail()+"\n";
		continut=continut+"Numar Telefon:"+client.getTelefon()+"\n";
		continut=continut+"\n";
		continut=continut+"Detalii comanda:"+"\n";
		continut=continut+"Nume produs:"+produs.getNume()+"\n";
		continut=continut+"Pret:"+produs.getPret()+" lei"+"\n";
		continut=continut+"Cantitate:"+cantitate+"\n";
		continut=continut+"Pret total:"+cantitate*produs.getPret()+" lei \n";
		return continut;
	}
	
}
