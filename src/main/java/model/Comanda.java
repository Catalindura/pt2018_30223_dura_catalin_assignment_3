package model;

public class Comanda {
	private int id;
	private int idClient;
	private int idProdus;
	private int cantitate;
	
	public Comanda() {
		
	}
	public Comanda(int comanda,int client,int produs,int cantitat) {
		super();
		id=comanda;
		idClient=client;
		idProdus=produs;
		cantitate=cantitat;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int x) {
		this.idClient=x;
	}
	public int getIdProdus() {
		return idProdus;
	}
	public void setIdProdus(int x) {
		this.idProdus=x;
	}
	public int getId() {
		return id;
	}
	public void setId(int x) {
		this.id=x;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int x) {
		this.cantitate=x;
	}
	public String toString() {
		return "IdComanda="+id+","+"IdClient="+idClient+","+"IdProdus="+idProdus+","+"Cantitate="+cantitate;
	}
}
