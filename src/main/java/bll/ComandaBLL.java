package bll;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import validators.CantitateValidator;
import validators.Validator;
import dao.ComandaDAO;
import model.Comanda;


public class ComandaBLL {
	private ComandaDAO comandaDAO;
	private List<Validator<Comanda>> validators;
	
	public ComandaBLL() {
		validators=new ArrayList<Validator<Comanda>>();
		validators.add(new CantitateValidator());
		comandaDAO=new ComandaDAO();
	}
	public Comanda findComandaById(int id) {
		Comanda st = comandaDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("Comanda cu id-ul:"+id+"nu a fost gasit");
		}
		return st;
	}
	public int deleteComandaById(int id) {
		int st=comandaDAO.deleteById(id);
		if(st==0) {
			throw new NoSuchElementException("Comanda cu id-ul:"+id+" nu a fost gasit");
		}
		return 1;
	}
	public Comanda UpdateComandaById(int id,Comanda t) {
		Comanda st=comandaDAO.update(t, id);
		if(st==null) {
			throw new NoSuchElementException("Comanda cu id-ul:"+id+" nu a fost gasit");
		}
		return st;
	}
	public int InsertComanda(Comanda t) {
		int sum=0;
		for (Validator<Comanda> v : validators) {
			sum=sum+v.validate(t);
		}
		if(sum==0) {
		 comandaDAO.insert(t);
		 return 1;
		}
		else return 0;
	}
	public List<Object> findAll() {
		List<Object> lista=new ArrayList<Object>();
		lista.addAll(comandaDAO.findAll());
		if(lista.size()==0) {
			throw new NoSuchElementException("Nu avem elemente in tabela");
		}
		return lista;
	}
	public static void main(String[] args){
		Comanda x=new Comanda(1,1,1,1);
		ComandaBLL test=new ComandaBLL();
		test.UpdateComandaById(1,x);
		System.out.println(x.toString());
	}
}
