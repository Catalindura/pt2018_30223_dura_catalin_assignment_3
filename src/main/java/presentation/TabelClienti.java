package presentation;
import java.awt.*;

import javax.swing.*;

class TabelClienti extends JFrame{
	private JTable tabel;
	public TabelClienti(JTable t){
		tabel=t;
		JScrollPane scroll=new JScrollPane(tabel);
		this.add(scroll);
		this.setSize(500,300);
		this.setVisible(true);
	}
}
