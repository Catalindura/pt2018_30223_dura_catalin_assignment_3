package bll;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import validators.EmailValidator;
import validators.TelefonValidator;
import validators.Validator;
import dao.ClientDAO;
import dao.ProdusDAO;
import model.Client;


public class ClientBLL {
	private ClientDAO clientDAO;
	private List<Validator<Client>> validators;
	
	public ClientBLL() {
		validators=new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new TelefonValidator());
		clientDAO=new ClientDAO();
	}
	public Client findClientById(int s1) {
		Client st = clientDAO.findById(s1);
		if (st == null) {
			throw new NoSuchElementException("Clientul cu id-ul:"+s1+"nu a fost gasit");
		}
		return st;
	}
	public int deleteClientById(int id) {
		int st=clientDAO.deleteById(id);
		if(st==0) return 0;
		else return 1;
	}
	public Client UpdateClientById(int id,Client t) {
		Client st=clientDAO.update(t, id);
		if(st==null) {
			throw new NoSuchElementException("Clientul cu id-ul:"+id+" nu a fost gasit");
		}
		return st;
	}
	public int InsertClient(Client t) {
		int sum=0;
		for (Validator<Client> v : validators) {
			sum=sum+v.validate(t);
		}
		if(sum==0) {
		 clientDAO.insert(t);
		 return 1;
		}
		else return 0;
	}
	public List<Object> findAll() {
		List<Object> lista=new ArrayList<Object>();
		lista.addAll(clientDAO.findAll());
		if(lista.size()==0) {
			throw new NoSuchElementException("Nu avem elemente in tabela");
		}
		return lista;
	}
	public static void main(String[] args){
		Client x=new Client(1,"Andrei","Alba","Calin@yahoo.com","0748372311");
		Client y=new Client(2,"Andrei","Alba","Andrei@yahoo.com","0748372323");
		ClientBLL test=new ClientBLL();
		Client t=test.findClientById(1);
		System.out.println(t.toString());
		List<Client> aux=new ArrayList<Client>();
		for(Client i:aux)
			System.out.println(i.toString());
	}
}
