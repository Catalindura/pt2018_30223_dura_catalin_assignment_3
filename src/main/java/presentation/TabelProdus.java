package presentation;
import java.awt.*;

import javax.swing.*;

class TabelProdus extends JFrame{
	private JTable tabel;
	public TabelProdus(JTable t){
		tabel=t;
		JScrollPane scroll=new JScrollPane(tabel);
		this.add(scroll);
		this.setSize(500,500);
		this.setVisible(true);
	}
}
