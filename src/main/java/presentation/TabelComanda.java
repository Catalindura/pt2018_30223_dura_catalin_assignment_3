package presentation;
import java.awt.*;

import javax.swing.*;

public class TabelComanda extends JFrame{
	private JTable tabel;
	public TabelComanda(JTable t){
		tabel=t;
		JScrollPane scroll=new JScrollPane(tabel);
		this.add(scroll);
		this.setSize(500,300);
		this.setVisible(true);
	}
}
