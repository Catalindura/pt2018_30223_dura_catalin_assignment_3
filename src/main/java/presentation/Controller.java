package presentation;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.swing.JTable;
import bill.*;
import bll.*;
import model.*;

public class Controller {
	private EntryView entryView=new EntryView();
	private ClientView clientView;
	private ProdusView produsView;
	private TabelClienti tabelClienti;
	private TabelProdus tabelProdus;
	private TabelComanda tabelComanda;
	private ComandaBLL comandaBLL=new ComandaBLL();
	private StocBLL stocBLL=new StocBLL();
	private ClientBLL clientBLL=new ClientBLL();
	private ProdusBLL produsBLL=new ProdusBLL();
	public Controller() {
	entryView.clientListener(new clientListener());
	entryView.produsListener(new produsListener());
	entryView.comandaListener(new comandaListener());
	entryView.facturaListener(new facturaListener());
	entryView.clearListener(new clearListener());
	entryView.listareComenziListener(new listareComenziListener());
	}
	
	class clientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			clientView=new ClientView();
			clientView.inserareClientListener(new inserareClientListener());
			clientView.stergereClientListener(new stergereClientListener());
			clientView.editClientListener(new editClientListener());
			clientView.listareClientiListener(new listareClientiListener());
			clientView.clearClientListener(new clearClientListener());
		}
		
	}
	class clearClientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			clientView.setIdClient();
			clientView.setNumeClient();
			clientView.setAdresClient();
			clientView.setEmailClient();
			clientView.setTelefonClient();
		}
		
	}
	class inserareClientListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		String s1=clientView.getIdClient();
		int id=Integer.parseInt(s1);
		String nume=clientView.getNumeClient();
		String adresa=clientView.getAdresClient();
		String email=clientView.getEmailClient();
		String telefon=clientView.getTelefonClient();
		Client inserat=new Client(id,nume,adresa,email,telefon);
		int test=clientBLL.InsertClient(inserat);
		if(test==1) { clientView.showMessage("Inserare realizata cu succes!");
					  List<Object> lista=clientBLL.findAll();
					  tabelClienti.setVisible(false);
					  tabelClienti=new TabelClienti(createJTable(lista));
		}
		else clientView.showMessage("Emailul sau numarul de telefon au fost introduse gresit");
	}	
	}
	
	class stergereClientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String s1=clientView.getIdClient();
			int id=Integer.parseInt(s1);
			Client cautat=clientBLL.findClientById(id);
			if(cautat!=null) {
			clientBLL.deleteClientById(id);
			clientView.showMessage("Stergere realizata cu succes!");
			List<Object> lista=clientBLL.findAll();
			tabelClienti.setVisible(false);
			tabelClienti=new TabelClienti(createJTable(lista));
	}
	}
	}	
	class editClientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String s1=clientView.getIdClient();
			int id=Integer.parseInt(s1);
			String nume=clientView.getNumeClient();
			String adresa=clientView.getAdresClient();
			String email=clientView.getEmailClient();
			String telefon=clientView.getTelefonClient();
			Client update=new Client(id,nume,adresa,email,telefon);
			Client test=clientBLL.findClientById(id);
			if(test!=null) {
			clientBLL.UpdateClientById(id, update);
			List<Object> lista=clientBLL.findAll();
			tabelClienti.setVisible(false);
			tabelClienti=new TabelClienti(createJTable(lista));
			clientView.showMessage("Clientul a fost editat cu succes!");
			}
		}
	}
	
	class produsListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			produsView=new ProdusView();
			produsView.inserareProdusListener(new inserareProdusListener());
			produsView.editProdusListener(new editProdusListener());
			produsView.stergereProdusListener(new stergereProdusListener());
			produsView.listProdusListener(new listProdusListener());
			produsView.clearProdusListener(new clearProdusListener());
		}
		
	}
	
	class clearProdusListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			produsView.setIdProdus();
			produsView.setPretProdus();
			produsView.setStoc();
			produsView.setNumeProdus();
		}
		
	}
	
	class listareClientiListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			List<Object> lista=clientBLL.findAll();
			tabelClienti=new TabelClienti(createJTable(lista));
		}
	
	}
	
	class listareComenziListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			List<Object> lista=comandaBLL.findAll();
			tabelComanda=new TabelComanda(createJTable(lista));
		}
	
	}
	
	class inserareProdusListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String s1=produsView.getIdProdus();
			int id=Integer.parseInt(s1);
			String nume=produsView.getNumeProdus();
			String s2=produsView.getPretProdus();
			int pret=Integer.parseInt(s2);
			String s3=produsView.getStoc();
			int stoc=Integer.parseInt(s3);
			Produs inserat=new Produs(id,nume,pret);
			Stoc insert=new Stoc(id,stoc);
			System.out.println(insert.toString());
			if(stoc>0) {
			int test=produsBLL.InsertProdus(inserat);
			stocBLL.InsertProdusStoc(insert);
			if(test==1) {
				produsView.showMessage("Produsul a fost inserat cu succes!");
				List<Object> lista=produsBLL.findAll();
				tabelProdus.setVisible(false);
				tabelProdus=new TabelProdus(createJTable(lista));
			}
			else produsView.showMessage("Produsul nu a putut fi inserat!");
			}
			else produsView.showMessage("Stocul este introdus gresit");
		
	}
	}
	class stergereProdusListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			String s1=produsView.getIdProdus();
			int id=Integer.parseInt(s1);
			Produs test=produsBLL.findProdusById(id);
			if(test!=null) {
			stocBLL.deleteProdusStocById(id);
			produsBLL.deleteProdusById(id);
			produsView.showMessage("Produsul a fost sters cu succes");
			List<Object> lista=produsBLL.findAll();
			tabelProdus.setVisible(false);
			tabelProdus=new TabelProdus(createJTable(lista));
		}
		}
	}
	class editProdusListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			String s1=produsView.getIdProdus();
			int id=Integer.parseInt(s1);
			String nume=produsView.getNumeProdus();
			String s2=produsView.getPretProdus();
			int pret=Integer.parseInt(s2);
			Produs test=produsBLL.findProdusById(id);
			if(test!=null) {
			Produs editat=new Produs(id,nume,pret);
			produsBLL.UpdateProdusById(id, editat);
			List<Object> lista=produsBLL.findAll();
			tabelProdus.setVisible(false);
			tabelProdus=new TabelProdus(createJTable(lista));
			produsView.showMessage("Produsul a fost editat cu succes!");
			}
		}
	}
	
	class listProdusListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			List<Object> lista=produsBLL.findAll();
			tabelProdus=new TabelProdus(createJTable(lista));
		}
	
	}
	
	class comandaListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String s1=entryView.getIdComanda();
			int id=Integer.parseInt(s1);
			String s2=entryView.getIdProdusComanda();
			int idProdus=Integer.parseInt(s2);
			String s3=entryView.getIdClientComanda();
			int idClient=Integer.parseInt(s3);
			String s4=entryView.getCantitate();
			int cantitate=Integer.parseInt(s4);
			Stoc s=stocBLL.findProdusStocById(idProdus);
			System.out.println(s.toString());
			if(s.getCantitateStoc()<cantitate) entryView.showMessage("Nu sunt atatea produse in stoc");
			else {
				entryView.showMessage("Comanda reusita");
				s.setCantitateStoc(s.getCantitateStoc()-cantitate);
				stocBLL.UpdateProdusStocById(idProdus, s);
				Comanda inserat=new Comanda(id,idClient,idProdus,cantitate);
				comandaBLL.InsertComanda(inserat);
				List<Object> lista=comandaBLL.findAll();
				tabelComanda.setVisible(false);
				tabelComanda=new TabelComanda(createJTable(lista));
			}
		}
		
		
	}
	
	class facturaListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String s1=entryView.getIdFactura();
			int idComanda=Integer.parseInt(s1);
			Comanda facturat=comandaBLL.findComandaById(idComanda);
			if(facturat!=null) {
			entryView.showMessage("Comanda Facturata");
			int idProdus=facturat.getIdProdus();
			int idClient=facturat.getIdClient();
			Produs produsFactura=produsBLL.findProdusById(idProdus);
			Client clientFactura=clientBLL.findClientById(idClient);
			Factura x=new Factura(clientFactura,produsFactura,facturat.getCantitate(),new Date());
			String factura=x.generareFactura();
			FileWriter file=null;
			comandaBLL.deleteComandaById(idComanda);
			try {
				file = new FileWriter("Factura id_"+idComanda+".txt");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			BufferedWriter buffer= new BufferedWriter(file);
			for (int i=0; i<factura.length(); i++) {
				if (factura.charAt(i) == '\n')
					try {
						buffer.newLine();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				else
					try {
						buffer.write(factura.charAt(i));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
			}
			if(buffer!=null)
				try {
					buffer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			if(file!=null)
				try {
					file.close();
				} catch (IOException e1) {
				
					e1.printStackTrace();
				}
		}
		}
	}
	
	class clearListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			entryView.SetIdCLientComanda();
			entryView.setIdComanda();
			entryView.setIdProdusComanda();
			entryView.SetCantitate();
			entryView.setIdFactura();
		}
		
	}
	
	public JTable createJTable(List<Object> objects) {
		int col=0;
		col=objects.get(0).getClass().getDeclaredFields().length;
		String[] coloane=new String[col];
		int i=0;
		String[][] matrice=new String[objects.size()][col];
		for(java.lang.reflect.Field field : objects.get(0).getClass().getDeclaredFields()) {
			coloane[i]=field.getName();
			i=i+1;
		}
	    i=0;
		for(Object t:objects) {
			int j=0;
			for(java.lang.reflect.Field field:t.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					Object aux=field.get(t);
					matrice[i][j]=aux.toString();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				j=j+1;
			}
			i=i+1;
		}
		JTable tabela=new JTable(matrice,coloane);
		return tabela;
	}
}
