package presentation;

import java.awt.event.ActionListener;

import javax.swing.*;

public class ClientView extends JFrame{
	private JButton clearClient=new JButton("Clear");
	private JButton insertClient=new JButton("Inserare Client");
	private JButton updateClient=new JButton("Editare Client");
	private JButton deleteClient=new JButton("Stergere Client");
	private JButton listClient=new JButton("Listare Clienti");
	private JTextField idClient=new JTextField(3);
	private JTextField numeClient=new JTextField(10);
	private JTextField adresaClient=new JTextField(10);
	private JTextField emailClient=new JTextField(20);
	private JTextField telefonClient=new JTextField(11);
	
	public ClientView() {
		JPanel p=new JPanel();
		p. setLayout (new BoxLayout (p, BoxLayout . Y_AXIS ));
		JPanel p1=new JPanel();
		JLabel l0=new JLabel("Client: id");
		JLabel l2=new JLabel("nume:");
		JLabel l3=new JLabel("adresa:");
		JLabel l4=new JLabel("email:");
		JLabel l5=new JLabel("telefon:");
		p1.add(l0);
		p1.add(idClient);
		JPanel p2=new JPanel();
		p2.add(l2);
		p2.add(numeClient);
		JPanel p3=new JPanel();
		p3.add(l3);
		p3.add(adresaClient);
		JPanel p4=new JPanel();
		p4.add(l4);
		p4.add(emailClient);
		JPanel p5=new JPanel();
		p5.add(l5);
		p5.add(telefonClient);
		JPanel p6=new JPanel();
		p6.add(insertClient);
		p6.add(updateClient);
		p6.add(deleteClient);
		p6.add(listClient);
		p.add(p6);
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.add(p4);
		p.add(p5);
		JPanel p7=new JPanel();
		p7.add(clearClient);
		p.add(p7);
		this.add(p);
		this.setSize(500,500);
		this.setVisible(true);
	}
	public String getIdClient() {
		return idClient.getText();
	}
	public void setIdClient() {
		idClient.setText("");
	}
	public String getNumeClient() {
		return numeClient.getText();
	}
	public void setNumeClient() {
		numeClient.setText("");
	}
	public String getAdresClient() {
		return adresaClient.getText();
	}
	public void setAdresClient() {
		adresaClient.setText("");
	}
	public String getEmailClient() {
		return emailClient.getText();
	}
	public void setEmailClient() {
		emailClient.setText("");
	}
	public String getTelefonClient() {
		return telefonClient.getText();
	}
	public void setTelefonClient() {
		telefonClient.setText("");
	}
	void clearClientListener(ActionListener mal) {
		clearClient.addActionListener(mal);
	}
	void inserareClientListener (ActionListener mal) {
		 insertClient.addActionListener(mal);
	}
	void stergereClientListener (ActionListener mal) {
		 deleteClient.addActionListener(mal);
	}
	void editClientListener (ActionListener mal) {
		 updateClient.addActionListener(mal);
	}
	void listareClientiListener (ActionListener mal) {
		 listClient.addActionListener(mal);
	}
	void showMessage(String s) {
		JOptionPane.showMessageDialog(this,s);
	}
}
