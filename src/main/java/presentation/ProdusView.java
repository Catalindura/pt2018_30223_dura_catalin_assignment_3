package presentation;

import java.awt.event.ActionListener;

import javax.swing.*;

public class ProdusView extends JFrame {
	private JTextField idProdus=new JTextField(3);
	private JTextField numeProdus=new JTextField(10);
	private JTextField pretProdus=new JTextField(6);
	private JTextField stocProdus=new JTextField(3);
	private JButton clearProdus=new JButton("Clear");
	private JButton insertProdus=new JButton("Inserare Produs");
	private JButton updateProdus=new JButton("Editare Produs");
	private JButton deleteProdus=new JButton("Stergere Produs");
	private JButton listProdus=new JButton("Listare Produse");
	
	public ProdusView() {
		JPanel p=new JPanel();
		p. setLayout (new BoxLayout (p, BoxLayout . Y_AXIS ));
		JPanel p1=new JPanel();
		JLabel l6=new JLabel(" Id:");
		JLabel l7=new JLabel("Nume:");
		JLabel l8=new JLabel("Pret:");
		JLabel l14=new JLabel(" stoc: ");
		p1.add(l6);
		p1.add(idProdus);
		JPanel p2=new JPanel();
		p2.add(l7);
		p2.add(numeProdus);
		JPanel p3=new JPanel();
		p3.add(l8);
		p3.add(pretProdus);
		JPanel p4=new JPanel();
		p4.add(l14);
		p4.add(stocProdus);
		JPanel p5=new JPanel();
		p5.add(insertProdus);
		p5.add(updateProdus);
		p5.add(deleteProdus);
		p5.add(listProdus);
		p.add(p5);
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.add(p4);
		JPanel p6=new JPanel();
		p6.add(clearProdus);
		p.add(p6);
		this.add(p);
		this.setSize(550,500);
		this.setVisible(true);
	}
	public String getIdProdus() {
		return idProdus.getText();
	}
	public void setIdProdus() {
		idProdus.setText("");
	}
	public String getNumeProdus() {
		return numeProdus.getText();
	}
	public void setNumeProdus() {
		numeProdus.setText("");
	}
	public String getPretProdus() {
		return pretProdus.getText();
	}
	public void setPretProdus() {
		pretProdus.setText("");
	}
	public String getStoc() {
		return stocProdus.getText();
	}
	public void setStoc() {
		stocProdus.setText("");
	}
	void showMessage(String s) {
		JOptionPane.showMessageDialog(this,s);
	}
	void clearProdusListener(ActionListener mal) {
		clearProdus.addActionListener(mal);
	}
	void inserareProdusListener (ActionListener mal) {
		 insertProdus.addActionListener(mal);
	}
	void stergereProdusListener (ActionListener mal) {
		 deleteProdus.addActionListener(mal);
	}
	void editProdusListener (ActionListener mal) {
		 updateProdus.addActionListener(mal);
	}
	void listProdusListener (ActionListener mal) {
		 listProdus.addActionListener(mal);
	}
}
