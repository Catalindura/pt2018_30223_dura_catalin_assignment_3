package validators;

import model.Produs;

public class PretValidator implements Validator<Produs> {
	
	public int validate(Produs t) {
		if(t.getPret()<=0)
			return 1;
		return 0;
	}
}